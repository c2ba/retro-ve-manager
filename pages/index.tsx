import { ConnectButton } from '@rainbow-me/rainbowkit';
import type { NextPage } from 'next';
import Head from 'next/head';
import styles from '../styles/Home.module.css';
import { useWalletClient, useAccount, usePublicClient, PublicClient } from 'wagmi'
import { useCallback, useEffect, useMemo, useState } from 'react';
import { WalletClient, Address, Hash, getContract, isAddressEqual, getAddress, formatUnits, parseUnits, parseEther } from 'viem'

import RetroVeManagerFactory from "contracts/abi/RetroVeManagerFactory.json";
import VotingEscrow from "contracts/abi/VotingEscrow.json";
import RetroVeManager from "contracts/abi/RetroVeManager.json";
import VoterV3 from "contracts/abi/VoterV3.json";
import UniswapV3Pool from "contracts/abi/UniswapV3Pool.json";
import ERC20 from "contracts/abi/ERC20.json";
import Image from 'next/image';

const ADDR_RETRO_VE_MANAGER_FACTORY = getAddress("0x3c9dC9Dd784173785b2A3fA041eA31D270C9c780");
const ADDR_VE_RETRO = getAddress("0xB419cE2ea99f356BaE0caC47282B9409E38200fa");
const ADDR_VOTER = getAddress("0xAcCbA5e852AB85E5E3a84bc8E36795bD8cEC5C73");
const ADDR_ZERO = getAddress("0x0000000000000000000000000000000000000000");

const STYLE_BUTTON = "bg-gray-300 hover:bg-gray-400 p-1";
const STYLE_SELECT = "bg-gray-300 cursor-pointer p-1";
const STYLE_TITLE = "text-3xl pb-2 border-y-2 border-gray-400 w-full text-center"

const range = (count: number | bigint) => Array.from((new Array(Number(count))).keys());

type ManagerInfo  = {
  address: Address,
  shouldMaxLock: boolean,
  approved: boolean,
  automationActive: boolean,
  currentTaskId: string;
  balance: bigint,
  tokenBalance: {[k: Address]: bigint}
}

type VeNFTInfo = {
  tokenId: bigint,
  managerInfo: ManagerInfo | undefined;
}

type TokenInfo = {
  name: string,
  symbol: string,
  decimals: bigint,
}

type VoterInfo = {
  pools: Address[],
  poolNames: Record<Address, string>,
  poolTokens: Record<Address, {token0: Address, token1: Address}>,
  gauges: Record<Address, Address>,
  tokens: Record<Address, TokenInfo>,
}

type OnchainData = {
  veNFTs: VeNFTInfo[],
  voterInfo: VoterInfo,
}

const Home: NextPage = () => {
  const publicClient = usePublicClient();
  const walletClientResult = useWalletClient();
  const walletClient = walletClientResult.data as WalletClient;
  const { address, isConnected } = useAccount();

  const [isReloading, setIsReloading] = useState<boolean>(false);
  const [unwatch, setUnwatch] = useState<() => any>();

  const [onchainData, setOnchainData] = useState<OnchainData | undefined>();
  const [selectedVeNFT, setSelectedVeNft] = useState<number | undefined>();
  const [withMaxLock, setWithMaxLock] = useState<boolean>(false);

  const loadVoterInfo = useCallback(async function() {
    const voter = {
      address: ADDR_VOTER,
      abi: VoterV3,
    };
    const poolLength = Number(await publicClient.readContract({...voter, functionName: "length"}) as bigint);
    let contractCalls: any = range(poolLength).map(
      poolIndex => (
        {
          ...voter,
          functionName: "pools",
          args: [poolIndex,],
        }
      ))
    let results = await publicClient.multicall({
      contracts: contractCalls,
    });
    const pools = results.map(result => result.result as Address);

    contractCalls = pools.map(
      poolAddr => (
          {
            address: poolAddr,
            abi: UniswapV3Pool,
            functionName: "token0",
          }
      )
    ).concat(
      pools.map(
        poolAddr => (
            {
              address: poolAddr,
              abi: UniswapV3Pool,
              functionName: "token1",
            }
        )
      )
    )

    results = await publicClient.multicall({
      contracts: contractCalls,
    });
    const poolTokens = Object.fromEntries(pools.map(
      (poolAddr, index) => [poolAddr, {token0: results[index].result as Address, token1: results[index + poolLength].result as Address}]
    ));

    const allTokens = Array.from(new Set(results.map(result => result.result as Address)));
    contractCalls = allTokens.map(
      tokenAddr => (
        {
          address: tokenAddr,
          abi: ERC20,
          functionName: "name",
        }
      )
    ).concat(
      allTokens.map(
        tokenAddr => (
          {
            address: tokenAddr,
            abi: ERC20,
            functionName: "decimals",
          }
        )
      )
    ).concat(
      allTokens.map(
        tokenAddr => (
          {
            address: tokenAddr,
            abi: ERC20,
            functionName: "symbol",
          }
        )
      )
    )
    results = await publicClient.multicall({
      contracts: contractCalls,
    });
    const tokenCount = allTokens.length;
    const tokens = Object.fromEntries(allTokens.map(
      (tokenAddr, index) => [tokenAddr, {name: results[index].result as string, decimals: results[index + tokenCount].result as bigint, symbol: results[index + 2 * tokenCount].result as string}]
    ));

    const poolNames = Object.fromEntries(pools.map(
      poolAddr => [poolAddr, `${tokens[poolTokens[poolAddr].token0].symbol}-${tokens[poolTokens[poolAddr].token1].symbol}`]
    ));

    contractCalls = pools.map(
      poolAddr => ({...voter, functionName: "gauges", args: [poolAddr,]})
    );
    results = await publicClient.multicall({ contracts: contractCalls});

    const gauges = Object.fromEntries(
      pools.map(
        (poolAddr, index) => [poolAddr, results[index].result as Address]
      )
    );
    return {
      pools,
      poolNames,
      poolTokens,
      gauges,
      tokens,
    }
  }, [publicClient]);

  const loadVeNFTs = useCallback(async function(tokens: Record<Address, TokenInfo>) {
    const veRETRO = getContract({
      address: ADDR_VE_RETRO,
      abi: VotingEscrow,
      publicClient,
    });
    const retroVeManagerFactory = getContract({
      address: ADDR_RETRO_VE_MANAGER_FACTORY,
      abi: RetroVeManagerFactory,
      publicClient,
    });
    const veBalance = await veRETRO.read.balanceOf([address,]) as bigint;
    const veNFTs = await Promise.all(range(veBalance).map(
      async index => {
        const tokenId = (await veRETRO.read.tokenOfOwnerByIndex([address, index,])) as bigint;
        const managerAddress = (await retroVeManagerFactory.read.tokenIdForManager([tokenId,])) as Address;
        const managerInfo = await (async () => {
          if (isAddressEqual(managerAddress, ADDR_ZERO)) {
            return undefined;
          }
          const manager = getContract({
            address: managerAddress,
            abi: RetroVeManager,
            publicClient,
          });
          const shouldMaxLock = (await manager.read.shouldMaxLock()) as boolean;
          const approved = (await veRETRO.read.isApprovedOrOwner([managerAddress, tokenId,])) as boolean;
          const automationActive = (await manager.read.automationActive()) as boolean;
          const currentTaskId = (await manager.read.currentTaskId()) as string;
          const balance = await publicClient.getBalance({address: managerAddress});

          const contractCalls = Object.keys(tokens).map(
            tokenAddress => {
              return {
                address: tokenAddress,
                abi: ERC20,
                functionName: "balanceOf",
                args: [managerAddress]
              }
            }
          ) as any;
          const callsResult = await publicClient.multicall({
            contracts: contractCalls,
          });
          const tokenBalance = Object.fromEntries(Object.keys(tokens).map(
            (tokenAddress, index) => [tokenAddress, callsResult[index].result as bigint]
          ))

          return {
            address: managerAddress,
            shouldMaxLock,
            approved,
            automationActive,
            currentTaskId,
            balance,
            tokenBalance,
          }
        })();
        return {
          tokenId,
          managerInfo,
        }
      }
    ));
    return veNFTs;
  }, [publicClient, address])

  const loadOnchainData = useCallback(async function(concurrentState: {active: boolean}) {
    if (!isConnected || address === undefined) {
      if (!concurrentState.active) {
        return;
      };
      setOnchainData(undefined);
      return;
    }
    console.debug("Loading onchain data...");
    setIsReloading(true);

    const voterInfo = await loadVoterInfo();
    const veNFTs = await loadVeNFTs(voterInfo.tokens);

    if (!concurrentState.active) {
      return;
    }
    console.debug("Loaded onchain data");
    setOnchainData({
      veNFTs,
      voterInfo,
    })
    setIsReloading(false);
  }, [isConnected, address, loadVeNFTs, loadVoterInfo]);

  const reloadOnchainData = useCallback(() => {
    if (unwatch !== undefined) {
      unwatch();
    }
    loadOnchainData({active: true});
  }, [unwatch, loadOnchainData]);

  useEffect(() => {
    if (unwatch !== undefined) {
      unwatch();
    }
    setUnwatch(publicClient.watchContractEvent({
      address: ADDR_RETRO_VE_MANAGER_FACTORY,
      abi: RetroVeManagerFactory,
      eventName: 'VeManagerCreated',
      onLogs: logs => {
        console.log(logs);
        reloadOnchainData();
      }
    }));
  }, [unwatch, setUnwatch, reloadOnchainData, publicClient])

  useEffect(
    () => {
      if (onchainData === undefined) {
        return;
      }
      if (onchainData.veNFTs.length == 0) {
        setSelectedVeNft(undefined);
        return;
      }
      if (selectedVeNFT !== undefined && selectedVeNFT < onchainData.veNFTs.length) {
        return;
      }
      setSelectedVeNft(0);
    }
  , [onchainData, selectedVeNFT])

  const waitTxHash = useCallback(async (hash: Hash) => {
    const maxTrials = 8;
    for (let i = 0; i < maxTrials; ++i) {
      try {
        setIsReloading(true);
        const transaction = await publicClient.waitForTransactionReceipt(
          { hash }
        );
        console.debug(transaction);
        return;
      } catch (e) {
        console.error(e);
      }
    }
    console.error(`Unable to find trsx hash after ${maxTrials} attempts`);
  }, [publicClient]);

  const createManager = useCallback(async (veNFTInfo: VeNFTInfo) => {
    const retroVeManagerFactory = getContract({
      address: ADDR_RETRO_VE_MANAGER_FACTORY,
      abi: RetroVeManagerFactory,
      publicClient,
      walletClient,
    });
    try {
      const hash = await retroVeManagerFactory.write.deployManager([veNFTInfo.tokenId, withMaxLock,]);
      await waitTxHash(hash);
    } catch(e) {
      console.error(e);
    }
    reloadOnchainData();
  }, [publicClient, walletClient, reloadOnchainData, withMaxLock, waitTxHash]);

  useEffect(() => {
    let concurrentState = { active: true }; // Reference: https://stackoverflow.com/questions/61751728/asynchronous-calls-with-react-usememo
    loadOnchainData(concurrentState);
    return () => { concurrentState.active = false; };
  }, [loadOnchainData]);

  const hasVeNFT = onchainData !== undefined && selectedVeNFT !== undefined;
  const veNFTInfo = hasVeNFT ? onchainData.veNFTs[selectedVeNFT] : undefined;

  return (
    <div className={styles.container}>
      <Head>
        <title>veRETRO Manager</title>
        <meta
          content="Generated by @rainbow-me/create-rainbowkit"
          name="description"
        />
        <link href="/favicon.ico" rel="icon" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"></link>
      </Head>

      <main>
        <div className="m-2 flex flex-row justify-between">
          <h1 className="text-3xl font-bold">
            veRETRO Manager - Beta UI{" "}
            <i className={`fa fa-refresh cursor-pointer ${isReloading ? "fa-spin" : ""}`} onClick={() => {
              reloadOnchainData();
            }}></i>
          </h1>
          <div className="right-0"><ConnectButton /></div>
        </div>
        { !hasVeNFT || veNFTInfo === undefined ? <>
            <p className={styles.description}>
              Please connect to Polygon network.
            </p>
          </> :
          <div className={styles.main}>
            <div className="flex flex-row gap-1 items-center">
              <label htmlFor="veNFT-select">Choose a veNFT:</label>
              <select
                name="veNFT-select"
                id="veNFT-select"
                title="veNFT-select"
                className={STYLE_SELECT}
                onChange={(event) => setSelectedVeNft(parseInt(event.target.value))}>
                {
                  onchainData.veNFTs.map(
                    (veNFTInfo, index) => {
                      const tokenId = Number(veNFTInfo.tokenId);
                      return <option key={tokenId} value={index} selected={index == selectedVeNFT}>{tokenId}</option>
                    }
                  )
                }
              </select>
            </div>
            {
              veNFTInfo.managerInfo === undefined ?
              <>
                <p className={styles.description}>This veNFT has no manager</p>
                <button className={STYLE_BUTTON} onClick={() => createManager(veNFTInfo)}>Create Manager</button>
                <div className="flex flex-row gap-2">
                  <input title="with-maxlock" type="checkbox" checked={withMaxLock} onChange={(event) => setWithMaxLock(Boolean(event.target.checked))}/>
                  <label htmlFor="with-maxlock" className="cursor-pointer" onClick={() => setWithMaxLock(!withMaxLock)}>With auto max lock ? (can be disabled later)</label>
                </div>
              </> :
              <ManagerUI publicClient={publicClient} walletClient={walletClient} reloadOnchainData={reloadOnchainData} waitTxHash={waitTxHash} tokenId={veNFTInfo.tokenId} managerInfo={veNFTInfo.managerInfo} voterInfo={onchainData.voterInfo} />
            }
          </div>
        }
      </main>

      <footer className={styles.footer}>
        <a href="https://optimismprime.io" rel="noopener noreferrer" target="_blank">
          Frontend made by Builder Bots at<Image className="ml-2 mr-2" alt="" src="/opp_logo.png" width="24" height="24" /> Optimism Prime
        </a>
      </footer>
    </div>
  );
};

type ManagerUIProps = {
  publicClient: PublicClient,
  walletClient: WalletClient,
  reloadOnchainData: () => any,
  waitTxHash: (hash: Hash) => Promise<any>,
  tokenId: bigint,
  managerInfo: ManagerInfo,
  voterInfo: VoterInfo,
}

const ManagerUI =  ({publicClient, walletClient, reloadOnchainData, waitTxHash, managerInfo, voterInfo, tokenId}: ManagerUIProps) => {
  const manager = getContract({
    address: managerInfo.address,
    abi: RetroVeManager,
    publicClient,
    walletClient,
  });
  const veNFT = getContract({
    address: ADDR_VE_RETRO,
    abi: VotingEscrow,
    publicClient,
    walletClient,
  });

  const toggleMaxLock = useCallback(async () => {
    try {
      const hash = await manager.write.setShouldMaxLock([!managerInfo.shouldMaxLock,]);
      await waitTxHash(hash);
    } catch(e) {
      console.error(e);
    }
    reloadOnchainData();
  }, [reloadOnchainData, waitTxHash, managerInfo, manager]);

  const setVote = useCallback(async (
    pools: Address[],
    weights: number[],
  ) => {
    try {
      const hash = await manager.write.vote([pools, weights,]);
      await waitTxHash(hash);
    } catch(e) {
      console.error(e);
    }
    reloadOnchainData();
  }, [reloadOnchainData, waitTxHash, manager]);

  const approve = useCallback(async () => {
    try {
      const hash = await veNFT.write.approve([managerInfo.address, tokenId,]);
      await waitTxHash(hash);
    } catch(e) {
      console.error(e);
    }
    reloadOnchainData();
  }, [reloadOnchainData, waitTxHash, managerInfo, tokenId, veNFT]);

  const revoke = useCallback(async () => {
    try {
      const hash = await veNFT.write.approve([ADDR_ZERO, tokenId,]);
      await waitTxHash(hash);
    } catch(e) {
      console.error(e);
    }
    reloadOnchainData();
  }, [reloadOnchainData, waitTxHash, tokenId, veNFT]);

  const approveToken = useCallback(async (tokenAddress: Address) => {
    try {
      const MAX_UINT256 = BigInt("115792089237316195423570985008687907853269984665640564039457584007913129639935");
      const token = getContract({
        address: tokenAddress,
        abi: ERC20,
        publicClient,
        walletClient,
      });
      const hash = await token.write.approve([managerInfo.address, MAX_UINT256,]);
      await waitTxHash(hash);
    } catch(e) {
      console.error(e);
    }
  }, [waitTxHash, managerInfo, publicClient, walletClient]);

  const withdrawToken = useCallback(async (tokenAddress: Address) => {
    try {
      const hash = await manager.write.withdrawTokens([tokenAddress, tokenAddress == ADDR_ZERO,]);
      await waitTxHash(hash);
    } catch(e) {
      console.error(e);
    }
    reloadOnchainData();
  }, [reloadOnchainData, waitTxHash, manager]);

  const depositToken = useCallback(async (tokenAddress: Address, amount: bigint) => {
    try {
      if (tokenAddress != ADDR_ZERO) {
        const token = getContract({
          address: tokenAddress,
          abi: ERC20,
          publicClient,
          walletClient,
        });
        const hash = await token.write.transfer([managerInfo.address, amount,]);
        await waitTxHash(hash);
      } else {
        const [account] = await walletClient.getAddresses()
        const hash = await walletClient.sendTransaction({
          account,
          to: managerInfo.address,
          value: amount,
        } as any);
        await waitTxHash(hash);
      }
    } catch(e) {
      console.error(e);
    }
    reloadOnchainData();
  }, [reloadOnchainData, waitTxHash, managerInfo, publicClient, walletClient]);

  const [reocurringBribes, setReocurringBribes] = useState<{[k: string]: ReocurringBribe}>({});
  const [selectedPoolsVotePercentages, setSelectedPoolsVotePercentages] = useState<{[k: string]: number}>({});
  const [automationProperties, setAutomationProperties] = useState({
    withBribe: false,
    withClaimAllAndVote: false,
    maticToDeposit: BigInt(0),
  });

  const createAutomation = useCallback(async () => {
    try {
      const pools = Array.from(new Set(Object.keys(reocurringBribes).map(
        poolAddressTokenAddress => poolAddressTokenAddress.split("-")[0]
      )));
      const bribesTokens = [] as Address[][];
      const amountToFundThisContractPerPool = [] as bigint[][];
      const weeklyAmounts = [] as bigint[][];
      for (const pool of pools) {
        const poolBribesTokens = [] as Address[];
        const poolAmountToFundThisContractPerPool = [] as bigint[];
        const poolWeeklyAmounts = [] as bigint[];
        for (const poolAddressTokenAddress in reocurringBribes) {
          if (!poolAddressTokenAddress.startsWith(pool)) {
            continue;
          }
          const {depositAmount, weeklyAmount} = reocurringBribes[poolAddressTokenAddress];
          poolBribesTokens.push(poolAddressTokenAddress.split("-")[1] as Address);
          poolAmountToFundThisContractPerPool.push(depositAmount);
          poolWeeklyAmounts.push(weeklyAmount)
        }
        bribesTokens.push(poolBribesTokens);
        amountToFundThisContractPerPool.push(poolAmountToFundThisContractPerPool);
        weeklyAmounts.push(poolWeeklyAmounts);
      }
      const noBribe = !automationProperties.withBribe;
      const hash = await manager.write.createAutomation([
        bribesTokens,
        pools,
        amountToFundThisContractPerPool,
        weeklyAmounts,
        noBribe,
        automationProperties.withClaimAllAndVote
      ], {value: automationProperties.maticToDeposit});
      await waitTxHash(hash);
    } catch (e) {
      console.error(e);
    }
    reloadOnchainData();
  }, [reloadOnchainData, waitTxHash, manager, automationProperties, reocurringBribes]);

  const [withdrawOnStopAutomation, setWithdrawOnStopAutomation] = useState<boolean>(true);
  const stopAutomation = useCallback(async () => {
    try {
      const hash = await manager.write.stopAutomation([
        withdrawOnStopAutomation
      ]);
      await waitTxHash(hash);
    } catch (e) {
      console.error(e);
    }
    reloadOnchainData();
  }, [reloadOnchainData, waitTxHash, manager, withdrawOnStopAutomation]);

  const updateAutomationWithBribeProperty = useCallback((newReocurringBribes: {[k: string]: ReocurringBribe}) => {
    if (Object.keys(newReocurringBribes).length > 0) {
      setAutomationProperties({...automationProperties, withBribe: true});
    } else {
      setAutomationProperties({...automationProperties, withBribe: false});
    }
  }, [setAutomationProperties, automationProperties]);

  const updateAutomationWithVoteAndClaimProperty = useCallback((newSelectedPoolsPercentages: {[k: string]: number}) => {
    if (Object.keys(newSelectedPoolsPercentages).length > 0) {
      setAutomationProperties({...automationProperties, withClaimAllAndVote: true});
    } else {
      setAutomationProperties({...automationProperties, withClaimAllAndVote: false});
    }
  }, [setAutomationProperties, automationProperties]);

  return <div className="flex flex-col gap-4">
    <div className="flex flex-col gap-2 items-center">
      <p className="text-2xl">
        Manager for this veNFT:{' '}
        <a href={`https://polygonscan.com/address/${managerInfo.address}`} rel="noopener noreferrer" target="_blank" className="text-sky-600">
          {managerInfo.address}
        </a>
      </p>
      <p className="italic">You need to approve the usage of your veNFT by the manager for vote, claim rebase & auto max lock to work.</p>
      {
        managerInfo.approved ?
        <>
          <p>Manager is approved.</p>
          <button className={STYLE_BUTTON} onClick={() => revoke()}>Revoke approval</button>
        </> :
        <>
          <p>Manager is not approved.</p>
          <button className={STYLE_BUTTON} onClick={() => approve()}>Approve</button>
        </>
      }
    </div>
    <div className="flex flex-row justify-center gap-2">
      <input title="with-maxlock" type="checkbox" checked={managerInfo.shouldMaxLock} onChange={() => toggleMaxLock()}/>
      <label htmlFor="with-maxlock" className="cursor-pointer" onClick={() => toggleMaxLock()}>With auto max lock (click to change)</label>
    </div>
    <div className="flex flex-col items-center gap-2">
      <h1 className={STYLE_TITLE}>About</h1>
      <div className="flex flex-col items-center italic">
        <p>
          veRETRO Manager is a tool to automate weekly tasks with your veRETRO NFT.
        </p>
        <p>
          Gelato network will run your automation each week. You can stop it or replace it anytime.
        </p>
      </div>
    </div>
    <ReoccuringBribeUI
      voterInfo={voterInfo}
      reocurringBribes={reocurringBribes}
      setReocurringBribes={(value) => { setReocurringBribes(value); updateAutomationWithBribeProperty(value); }}
      approveToken={approveToken}
    />
    <VoteUI
      selectedPoolsPercentages={selectedPoolsVotePercentages}
      setSelectedPoolsPercentages={(value) => { setSelectedPoolsVotePercentages(value); updateAutomationWithVoteAndClaimProperty(value); }}
      voterInfo={voterInfo}
      setVote={setVote}
    />
    <div className="flex flex-col items-center gap-2">
    <h1 className={STYLE_TITLE}>Automation</h1>
      <div className="flex flex-row gap-2">
        <input
          title="with-vote-and-claim"
          type="checkbox"
          checked={automationProperties.withClaimAllAndVote}
          onChange={() => setAutomationProperties({...automationProperties, withClaimAllAndVote: !automationProperties.withClaimAllAndVote})}
        />
        <label
          htmlFor="with-vote-and-claim"
          className="cursor-pointer"
          onClick={() => setAutomationProperties({...automationProperties, withClaimAllAndVote: !automationProperties.withClaimAllAndVote})}
        >
          With vote & claim rebase
        </label>
      </div>
      <div className="flex flex-row gap-2">
        <input
          title="with-bribe"
          type="checkbox"
          checked={automationProperties.withBribe}
          onChange={() => setAutomationProperties({...automationProperties, withBribe: !automationProperties.withBribe})}
        />
        <label
          htmlFor="with-bribe"
          className="cursor-pointer"
          onClick={() => setAutomationProperties({...automationProperties, withBribe: !automationProperties.withBribe})}
        >
          With reocurring bribe
        </label>
      </div>
      <div className="flex flex-row items-center gap-2 justify-center">
        <p>MATIC to deposit:</p>
        <TokenAmountInput
          decimals={BigInt(18)}
          value={automationProperties.maticToDeposit}
          setValue={(value) => setAutomationProperties({...automationProperties, maticToDeposit: value})}
          title="deposit-matic"
        />
      </div>
    </div>
    <button className={STYLE_BUTTON} onClick={() => createAutomation()}>{managerInfo.automationActive ? "Replace" : "Create"} automation</button>
    {
      managerInfo.automationActive ?
      <div className="flex flex-col items-center gap-2">
        <h1 className={STYLE_TITLE}>Current Automation</h1>
        <a href={`https://app.gelato.network/task/${managerInfo.currentTaskId}?chainId=137`} rel="noopener noreferrer" target="_blank" className="text-sky-600">
          https://app.gelato.network/task/{managerInfo.currentTaskId}?chainId=137
        </a>
        <button className={STYLE_BUTTON} onClick={() => stopAutomation()}>Stop automation</button>
        <div className="flex flex-row gap-2">
          <input
              title="with-withdraw-on-stop-automation"
              type="checkbox"
              checked={withdrawOnStopAutomation}
              onChange={() => setWithdrawOnStopAutomation(!withdrawOnStopAutomation)}
            />
          <label
            htmlFor="with-withdraw-on-stop-automation"
            className="cursor-pointer"
            onClick={() => setWithdrawOnStopAutomation(!withdrawOnStopAutomation)}
          >
            Withdraw all tokens & matic
          </label>
        </div>
      </div>
      : <></>
    }
    <DepositWithdrawUI
      managerInfo={managerInfo}
      voterInfo={voterInfo}
      depositToken={depositToken}
      withdrawToken={withdrawToken}
    />
  </div>
}

type DepositWithdrawUIProps = {
  managerInfo: ManagerInfo,
  voterInfo: VoterInfo,
  withdrawToken: (address: Address) => any,
  depositToken: (address: Address, amount: bigint) => any,
}

const DepositWithdrawUI = (
  {
    managerInfo,
    voterInfo,
    withdrawToken,
    depositToken,
  }
  : DepositWithdrawUIProps
) => {
  const [maticToDeposit, setMaticToDeposit] = useState<bigint>(BigInt(0));
  const [tokenAmountToDeposit, setTokenAmountToDeposit] = useState<bigint>(BigInt(0));
  const [selectedToken, setSelectedToken] = useState<number>(0);

  const token = useMemo(() => {
    const tokenAddress = Object.keys(voterInfo.tokens)[selectedToken] as Address;
    return {
      ...voterInfo.tokens[tokenAddress],
      address: tokenAddress,
    }
  }, [selectedToken, voterInfo])

  return (
    <div className="flex flex-col items-center gap-2">
      <h1 className={STYLE_TITLE}>Deposit Gas for Automation</h1>
      <div className="flex flex-col items-center italic">
        <p>
          You need to deposit MATIC on the manager to pay for automation fees. You can withdraw anytime.
        </p>
        <p>
          You can also deposit when creating the automation, see {'"'}Automation{'"'} section.
        </p>
      </div>
      <div className="flex flex-row items-center justify-center gap-2 w-full">
        <p>Manager balance: {formatUnits(managerInfo.balance, 18)} MATIC</p>
        <button className={STYLE_BUTTON} onClick={() => withdrawToken(ADDR_ZERO)}>Withdraw</button>
      </div>
      <div className="flex flex-row items-center gap-2">
        <TokenAmountInput
          decimals={BigInt(18)}
          value={maticToDeposit}
          setValue={setMaticToDeposit}
          title="deposit-matic"
        />
        <button className={STYLE_BUTTON} onClick={() => depositToken(ADDR_ZERO, maticToDeposit)}>Deposit</button>
      </div>
      <h1 className={STYLE_TITLE}>Deposit Bribes</h1>
      <div className="flex flex-col items-center italic">
        <p>
          You need to deposit tokens on the manager to pay for weekly reoccuring bribes. You can withdraw anytime.
        </p>
        <p>
          You can also deposit when creating the automation, see {'"'}Reoccuring Bribe{'"'} section.
        </p>
      </div>
      <div className="flex flex-row items-center gap-2">
        <p>Token:</p>
        <select
          id="deposit-token-select"
          title="deposit-token-select"
          className={STYLE_SELECT}
          onChange={(event) => {
            setSelectedToken(parseInt(event.target.value));
            setTokenAmountToDeposit(BigInt(0));
          }}>
          {
            Object.entries(voterInfo.tokens).map(
              (keyValue, index) => {
                return <option
                  key={keyValue[0]}
                  value={index}
                  selected={index == selectedToken}
                >
                  {keyValue[1].symbol} ({formatUnits(managerInfo.tokenBalance[keyValue[0] as Address], Number(keyValue[1].decimals))})
                </option>
              }
            )
          }
        </select>
      </div>
      <div className="flex flex-row items-center justify-center gap-2 w-full">
        <p>Manager balance: {formatUnits(managerInfo.tokenBalance[token.address], Number(token.decimals))} {token.symbol}</p>
        <button className={STYLE_BUTTON} onClick={() => withdrawToken(token.address)}>Withdraw</button>
      </div>
      <div className="flex flex-row items-center gap-2">
        <TokenAmountInput
          decimals={token.decimals}
          value={tokenAmountToDeposit}
          setValue={setTokenAmountToDeposit}
          title="deposit-token"
        />
        <button className={STYLE_BUTTON} onClick={() => depositToken(token.address, tokenAmountToDeposit)}>Deposit</button>
      </div>
    </div>
  )
}

type ReocurringBribe = {
  depositAmount: bigint,
  weeklyAmount: bigint,
}

type ReoccuringBribeUIProps = {
  voterInfo: VoterInfo,
  reocurringBribes: {[k: string]: ReocurringBribe},
  setReocurringBribes: (arg: {[k: string]: ReocurringBribe}) => any,
  approveToken: (address: Address) => any,
}

const ReoccuringBribeUI = ({voterInfo, reocurringBribes, setReocurringBribes, approveToken}: ReoccuringBribeUIProps) => {
  const [selectedPool, setSelectedPool] = useState<number>(0);
  const [selectedToken, setSelectedToken] = useState<number>(0);
  return <div className="flex flex-col items-center gap-2">
    <h1 className={STYLE_TITLE}>Reoccuring Bribe</h1>
    <div className="flex flex-col items-center italic">
      <p>
        You can choose a list of weekly reoccuring bribes. Click on {'"'}Add to selection{'"'} to build the list.
      </p>
      <p>
        You can deposit several bribes at once when creating the automation but you need to approve each token.
      </p>
      <p>
        You can also deposit later, see {'"'}Deposit Bribes{'"'} section below.
      </p>
      <p>
        If you use this feature do not forget to check {'"'}With reocurring bribe{'"'} below
      </p>
    </div>
    <div className="flex flex-row gap-2">
      <select
        id="bribe-pool-select"
        title="bribe-pool-select"
        className={STYLE_SELECT}
        onChange={(event) => setSelectedPool(parseInt(event.target.value))}>
        {
          voterInfo.pools.map(
            (poolAddress, index) => {
              return <option key={poolAddress} value={index} selected={index == selectedPool}>{voterInfo.poolNames[poolAddress]}</option>
            }
          )
        }
      </select>
      <select
        id="bribe-token-select"
        title="bribe-token-select"
        className={STYLE_SELECT}
        onChange={(event) => setSelectedToken(parseInt(event.target.value))}>
        {
          Object.entries(voterInfo.tokens).map(
            (keyValue, index) => {
              return <option key={keyValue[0]} value={index} selected={index == selectedToken}>{keyValue[1].symbol}</option>
            }
          )
        }
      </select>
      <button
        className={STYLE_BUTTON}
        onClick={
          () => setReocurringBribes({
            ...reocurringBribes,
            [`${voterInfo.pools[selectedPool]}-${Object.keys(voterInfo.tokens)[selectedToken]}`]: {
              depositAmount: BigInt(0),
              weeklyAmount: BigInt(0),
            },
          })
        }
      >
        Add to selection
      </button>
    </div>
    { Object.keys(reocurringBribes).length == 0 ? <></> :
      <>
        <h2 className="text-2xl">Selected Bribes:</h2>
        <div className="w-full flex flex-col gap-2">
          <div className="flex flex-row items-center gap-2 w-full font-bold">
            <div className="w-1/5">Pool</div>
            <div className="w-1/6">Token</div>
            <div className="w-1/3">Amount to deposit</div>
            <div className="w-1/3">Amount to bribe weekly</div>
            <div className="w-1/6"></div>
            <div className="w-1/6"></div>
          </div>
          {
            Object.keys(reocurringBribes).map(poolAddrTokenAddr => {
              const [poolAddr, tokenAddr] = poolAddrTokenAddr.split("-");
              return (
                <div key={poolAddrTokenAddr} className="flex flex-row items-center gap-2 w-full">
                  <div className="w-1/5">{voterInfo.poolNames[poolAddr as Address]}</div>
                  <div className="w-1/6">{voterInfo.tokens[tokenAddr as Address].symbol}</div>
                  <div className="w-1/3">
                    <TokenAmountInput
                      decimals={voterInfo.tokens[tokenAddr as Address].decimals}
                      value={reocurringBribes[poolAddrTokenAddr].depositAmount}
                      setValue={
                        (value) => setReocurringBribes({
                          ...reocurringBribes,
                          [poolAddrTokenAddr]: {
                            ...reocurringBribes[poolAddrTokenAddr],
                            depositAmount: value,
                          },
                        })
                      }
                      title={poolAddr}
                    />
                  </div>
                  <div className="w-1/3">
                    <TokenAmountInput
                      decimals={voterInfo.tokens[tokenAddr as Address].decimals}
                      value={reocurringBribes[poolAddrTokenAddr].weeklyAmount}
                      setValue={
                        (value) => {
                          setReocurringBribes({
                          ...reocurringBribes,
                          [poolAddrTokenAddr]: {
                            ...reocurringBribes[poolAddrTokenAddr],
                            weeklyAmount: value,
                          },
                        })}
                      }
                      title={poolAddr}
                    />
                  </div>
                  <div className="w-1/6">
                    <button
                      className={STYLE_BUTTON}
                      onClick={() => approveToken(poolAddrTokenAddr.split("-")[1] as Address)}
                    >
                      Approve
                    </button>
                  </div>
                  <div className="w-1/6">
                    <button
                      className={STYLE_BUTTON}
                      onClick={() => {
                        const {[poolAddrTokenAddr]: _, ...newObject} = reocurringBribes;
                        setReocurringBribes(newObject);
                      }}
                    >
                      Remove
                    </button>
                  </div>
                </div>
            )})
          }
        </div>
      </>
    }
  </div>
}

type VoteUIProps = {
  voterInfo: VoterInfo,
  selectedPoolsPercentages: {[k: string]: number},
  setSelectedPoolsPercentages: (arg: {[k: string]: number}) => any,
  setVote: (pools: Address[], weights: number[]) => any,
}

const VoteUI = (
  {selectedPoolsPercentages, setSelectedPoolsPercentages, voterInfo, setVote}: VoteUIProps
) => {
  const [selectedPool, setSelectedPool] = useState<number>(0);
  return <div className="flex flex-col items-center gap-2">
    <h1 className={STYLE_TITLE}>Vote</h1>
    <div className="flex flex-col items-center italic">
      <p>
        You can choose a list of pools to vote for. Click on {'"'}Add to selection{'"'} to build the list.
      </p>
      <p>
        Clicking on {'"'}Set vote & claim rebase{'"'} will cast the vote and claim your rebase.
      </p>
      <p>
        If you want the vote to be casted each week, create an automation with {'"'}vote & claim rebase{'"'} enabled.
      </p>
    </div>
    <div className="flex flex-row gap-2">
      <select
        id="vote-pool-select"
        title="vote-pool-select"
        className={STYLE_SELECT}
        onChange={(event) => setSelectedPool(parseInt(event.target.value))}>
        {
          voterInfo.pools.map(
            (poolAddress, index) => {
              return <option key={poolAddress} value={index} selected={index == selectedPool}>{voterInfo.poolNames[poolAddress]}</option>
            }
          )
        }
      </select>
      <button
        className={STYLE_BUTTON}
        onClick={
          () => setSelectedPoolsPercentages({
            ...selectedPoolsPercentages,
            [voterInfo.pools[selectedPool]]: 0,
          })
        }
      >
        Add to selection
      </button>
    </div>
    { Object.keys(selectedPoolsPercentages).length == 0 ? <></> :
      <>
        <h2 className="text-2xl">Selected Pools:</h2>
        <div className="w-full flex flex-col gap-2">
        <div className="flex flex-row items-center justify-center gap-2 w-full font-bold">
            <div className="w-1/3">Pool</div>
            <div className="w-1/4">Vote weight</div>
            <div className="w-1/4"></div>
          </div>
          {
            Object.keys(selectedPoolsPercentages).map(poolAddr => (
              <div key={poolAddr} className="flex flex-row items-center justify-center gap-2 w-full">
                <div className="w-1/3">{voterInfo.poolNames[poolAddr as Address]}</div>
                <div className="w-1/4">
                  <PoolWeightInput
                    value={selectedPoolsPercentages[poolAddr]}
                    setValue={
                      (value) => setSelectedPoolsPercentages({
                        ...selectedPoolsPercentages,
                        [poolAddr]: value,
                      })
                    }
                    title={poolAddr}
                  />
                </div>
                <div className="w-1/4">
                  <button
                    className={STYLE_BUTTON}
                    onClick={()=>{
                      const {[poolAddr]: _, ...newObject} = selectedPoolsPercentages;
                      setSelectedPoolsPercentages(newObject);
                    }}
                  >
                    Remove
                  </button>
                </div>
              </div>
            ))
          }
        </div>
        <button
          className={STYLE_BUTTON}
          onClick={
            () => {
              setVote(
                Object.keys(selectedPoolsPercentages) as [Address],
                Object.values(selectedPoolsPercentages),
              )
            }
          }
        >
          Set vote & claim rebase
        </button>
      </>
    }
  </div>
}

type PoolWeightInputProps = {
  value: number,
  setValue: (arg: number) => any,
  title: string,
}
const PoolWeightInput = ({value, setValue, title}: PoolWeightInputProps) => {
  return <input
    className="border-2 border-black text-right w-full"
    type="number"
    value={value}
    min="0"
    max="100"
    onChange={
      (e) => setValue(Math.max(0, Math.min(100, parseInt(e.target.value))))
    }
    title={title}
  />
}

type TokenAmountInputProps = {
  decimals: bigint,
  value: bigint,
  setValue: (arg: bigint) => any,
  title: string,
}
const TokenAmountInput = ({decimals, value, setValue, title}: TokenAmountInputProps) => {
  const [internalValue, setInternalValue] = useState<string>(formatUnits(value, Number(decimals)));
  return <input
    className="border-2 border-black text-right w-full"
    type="number"
    value={internalValue}
    min="0"
    onChange={
      (e) => {
        const clamped = parseFloat(e.target.value) >= 0 ? e.target.value : Math.max(0, parseFloat(e.target.value)).toString();
        setInternalValue(clamped);
        try {
          setValue(parseUnits(clamped, Number(decimals)))
        } catch (e) {
          console.debug(e);
        }
      }
    }
    title={title}
  />
}

export default Home;
