import '../styles/globals.css';
import '@rainbow-me/rainbowkit/styles.css';
import { RainbowKitProvider } from '@rainbow-me/rainbowkit';
import type { AppProps } from 'next/app';
import { configureChains, createConfig, WagmiConfig } from 'wagmi';
import {
  injectedWallet,
  rainbowWallet,
  walletConnectWallet,
} from '@rainbow-me/rainbowkit/wallets';
import { connectorsForWallets } from '@rainbow-me/rainbowkit';
import {
  polygon,
} from 'wagmi/chains';
import { publicProvider } from 'wagmi/providers/public';
import { jsonRpcProvider } from 'wagmi/providers/jsonRpc'

const { chains, publicClient, webSocketPublicClient } = configureChains(
  [
    polygon,
  ],
  [
    publicProvider(),
    jsonRpcProvider({
      rpc: (chain) => ({
        http: `https://polygon.llamarpc.com`,
      }),
    }),
    jsonRpcProvider({
      rpc: (chain) => ({
        http: `https://polygon-rpc.com`,
      }),
    }),
    jsonRpcProvider({
      rpc: (chain) => ({
        http: `https://rpc.ankr.com/polygon`,
      }),
    }),
  ],
);

const connectors = connectorsForWallets([
  {
    groupName: 'Recommended',
    wallets: [
      injectedWallet({ chains }),
      // rainbowWallet({ projectId: "YOU_PROJECT_ID", chains }),
      // walletConnectWallet({ projectId: "YOU_PROJECT_ID", chains }),
    ],
  },
]);

const wagmiConfig = createConfig({
  autoConnect: true,
  connectors,
  publicClient,
  webSocketPublicClient,
});

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <WagmiConfig config={wagmiConfig}>
      <RainbowKitProvider chains={chains}>
        <Component {...pageProps} />
      </RainbowKitProvider>
    </WagmiConfig>
  );
}

export default MyApp;
