// SPDX-License-Identifier: BUSL-1.1
pragma solidity 0.8.19;

import {IERC20} from "@openzeppelin/contracts/token/ERC20/ERC20.sol";
import {SafeERC20} from "@openzeppelin/contracts/token/ERC20/utils/SafeERC20.sol";
import {Initializable} from "@openzeppelin/contracts/proxy/utils/Initializable.sol";

import {IVoter} from "./interfaces/IVoter.sol";
import {IVotingEscrow} from "./interfaces/IVotingEscrow.sol";
import {IRewardsDistributor} from "./interfaces/IRewardsDistributor.sol";
import {IMinter} from "./interfaces/IMinter.sol";
import {IBribe} from "./interfaces/IBribe.sol";
import {IOps} from "./interfaces/IOps.sol";
import {IOpsProxyFactory} from "./interfaces/IOpsProxyFactory.sol";
import {Types} from "./Types.sol";

// Contract to manager both bribe distribution and a users venft.
contract RetroVeManager is Initializable, Types {
    using SafeERC20 for IERC20;

    uint256 private constant MAX_TIME = 2 * 365 * 86400;
    address private constant MATIC = 0xEeeeeEeeeEeEeeEeEeEeeEEEeeeeEeeeeeeeEEeE;
    uint256 private constant WEEK = 1 weeks;

    // Address needed
    IVoter public constant voter =
        IVoter(0xAcCbA5e852AB85E5E3a84bc8E36795bD8cEC5C73);
    IVotingEscrow public constant veNft =
        IVotingEscrow(0xB419cE2ea99f356BaE0caC47282B9409E38200fa);
    IRewardsDistributor public constant rewardsDistributor =
        IRewardsDistributor(0x072ba11A17ac13EfF9F566d6d54f4887BAB94d3C);
    IMinter public constant minter =
        IMinter(0x003D505Aff54FB7856aA6Bcb56a8397F5aF89479);
    IOps public constant ops = IOps(0x527a819db1eb0e34426297b03bae11F2f8B3A19E);
    IOpsProxyFactory public constant opsProxyFactory =
        IOpsProxyFactory(0xC815dB16D4be6ddf2685C201937905aBf338F5D7);

    bytes32 public currentTaskId;
    uint256 public tokenId;
    uint256 public lastExecution;
    bool public shouldMaxLock;
    bool public automationActive;
    address[] public bribeTokens;

    // Use storage to push dynamic arrays
    uint256[] private _claimAmounts;
    address[] private _claimTokens;

    struct ReoccuringBribe {
        address[] pools;
        address[][] tokens;
        uint256[][] amounts;
        bool noBribe;
        bool withClaimAndVote;
    }

    ReoccuringBribe public reoccuringBribe;

    struct Vote {
        uint256 voteTime;
        address[] poolVote;
        uint256[] weights;
    }

    Vote public currentVote;

    struct LastBribe {
        address[][] tokens;
        address[] pools;
    }

    mapping(uint256 => Vote) public periodVote;
    mapping(address => bool) private bribeTokenExists;

    /// ============ ERRORS ============ ///
    error NotAuth();
    error NativeTransferFailed();
    error GelatoHungry();

    /// ============ EVENTS ============ ///
    event Voted(uint256 activePeriod, address[] poolVotes, uint256[] weights);
    event Bribed(address pool, address token, uint256 amount);
    event RewardsClaimed(address bribe, address[] tokens, uint256[] amounts);
    event IncreasedLock();
    event EmissionsClaimed(uint256 amount);

    function initialize(
        uint256 _tokenId,
        bool _shouldMaxLock
    ) external initializer {
        tokenId = _tokenId;
        shouldMaxLock = _shouldMaxLock;
    }

    modifier onlyOwner() {
        _onlyOwner();
        _;
    }

    modifier onlyGelato() {
        _onlyGelato();
        _;
    }

    function _onlyOwner() private view {
        if (!veNft.isApprovedOrOwner(msg.sender, tokenId)) revert NotAuth();
    }

    function _onlyGelato() private view {
        if (msg.sender != execAddress()) revert NotAuth();
    }

    function execAddress() public view returns (address _executor) {
        (_executor, ) = opsProxyFactory.getProxyOf(address(this));
    }

    /// ============ CLAIM ALL AND VOTE ============ ///
    /**@notice Claim emissions, increase lock, get all previous bribe rewards and vote the same as last week */
    function claimAllAndVote() external onlyOwner {
        _claimAllAndVote();
    }

    function _claimAllAndVote() private {
        _claimEmissions();
        _increaseUnlockTime();
        _getRewards();
        _vote(currentVote.poolVote, currentVote.weights);
    }

    /// ============ GET REWARDS ============ ///
    /**@notice Claim rewards for a specific bribe or fee gauge */
    function getRewards(address _bribeOrFeeGauge) external onlyOwner {
        _getRewards(_bribeOrFeeGauge);
    }

    function getRewards(
        address _bribeOrFeeGauge,
        address[] calldata _tokens
    ) external onlyOwner {
        _getRewards(_bribeOrFeeGauge, _tokens);
    }

    /**@notice Claim all rewards for current vote */
    function getRewards() external onlyOwner {
        _getRewards();
    }

    function _getRewards() private {
        address[] memory _pools = currentVote.poolVote;
        for (uint i; i < _pools.length; ++i) {
            address _gauge = voter.gauges(_pools[i]);
            address _bribes = voter.external_bribes(_gauge);
            address _fees = voter.internal_bribes(_gauge);

            _getRewards(_bribes);
            _getRewards(_fees);
        }
    }

    function _getRewards(address _bribes) private {
        uint len = IBribe(_bribes).rewardsListLength();
        for (uint i; i < len; ++i) {
            address token = IBribe(_bribes).rewardTokens(i);
            uint256 amt = IBribe(_bribes).earned(tokenId, token);
            if (amt > 0) {
                _claimTokens.push(token);
                _claimAmounts.push(amt);
            }
        }

        if (_claimTokens.length > 0) {
            IBribe(_bribes).getReward(tokenId, _claimTokens);
            emit RewardsClaimed(_bribes, _claimTokens, _claimAmounts);

            // Reset storage
            delete _claimTokens;
            delete _claimAmounts;
        }
    }

    function _getRewards(address _bribes, address[] calldata _tokens) private {
        uint len = _tokens.length;
        for (uint i; i < len; ++i) {
            uint256 amt = IBribe(_bribes).earned(tokenId, _tokens[i]);
            if (amt > 0) {
                _claimTokens.push(_tokens[i]);
                _claimAmounts.push(amt);
            }
        }

        if (_claimTokens.length > 0) {
            IBribe(_bribes).getReward(tokenId, _claimTokens);
            emit RewardsClaimed(_bribes, _claimTokens, _claimAmounts);

            // Reset storage
            delete _claimTokens;
            delete _claimAmounts;
        }
    }

    /// ============ BRIBE ============ ///
    function bribe(
        address _token,
        address _pool,
        uint256 _amount,
        bool _fromBalance
    ) external onlyOwner {
        if (bribeTokenExists[_token])
            _bribe(_token, _pool, _amount, _fromBalance);
        else {
            _initBribeToken(_token);
            _bribe(_token, _pool, _amount, _fromBalance);
        }
    }

    function _initBribeToken(address _token) private {
        bribeTokenExists[_token] = true;
        bribeTokens.push(_token);
    }

    function _bribe(
        address _token,
        address _pool,
        uint256 _amount,
        bool _fromBalance
    ) private {
        if (!_fromBalance)
            IERC20(_token).safeTransferFrom(msg.sender, address(this), _amount);

        // How much bribe token do we have on hand? Let's set that to amount.
        uint256 bal = IERC20(_token).balanceOf(address(this));
        if (bal < _amount) _amount = bal;

        // Bribe the voterrrsss
        address gauge = voter.gauges(_pool);
        address _bribeGauge = voter.external_bribes(gauge);

        _approveTokenIfNeeded(_token, _bribeGauge);
        if (_amount > 0)
            IBribe(_bribeGauge).notifyRewardAmount(_token, _amount);
        emit Bribed(_pool, _token, _amount);
    }

    /// ============ VOTE ============ ///
    /**@notice Vote for the same pools and weights as the previous vote */
    function vote() external onlyOwner {
        _claimEmissions();
        _increaseUnlockTime();
        _vote(currentVote.poolVote, currentVote.weights);
    }

    /**@notice Set a new vote */
    function vote(
        address[] calldata _poolVote,
        uint256[] calldata _weights
    ) external onlyOwner {
        _claimEmissions();
        _increaseUnlockTime();
        _vote(_poolVote, _weights);
    }

    function _vote(
        address[] memory _poolVote,
        uint256[] memory _weights
    ) private {
        Vote memory _currentVote = Vote(block.timestamp, _poolVote, _weights);
        currentVote = _currentVote;
        uint256 activePeriod = IMinter(minter).active_period();
        periodVote[activePeriod] = _currentVote;

        voter.vote(tokenId, _poolVote, _weights);
        emit Voted(activePeriod, _poolVote, _weights);
    }

    /// ============ CLAIM EMISSIONS ============ ///
    /**@notice Emissions are claimed in the vote function, but can claim them here if needed */
    function claimEmissions() external onlyOwner {
        _claimEmissions();
    }

    function _claimEmissions() private {
        uint256 amount = rewardsDistributor.claimable(tokenId);
        if (amount > 0) {
            rewardsDistributor.claim(tokenId);
            emit EmissionsClaimed(amount);
        }
    }

    /// ============ INCREASE UNLOCK TIME ============ ///
    /**@notice Max lock our veNFT */
    function increaseUnlockTime() external onlyOwner {
        _increaseUnlockTime();
    }

    function _increaseUnlockTime() private {
        (, uint256 end) = veNft.locked(tokenId);
        uint256 diff = MAX_TIME - (end - block.timestamp);
        if (shouldMaxLock && diff > 1 weeks) {
            veNft.increase_unlock_time(tokenId, MAX_TIME);
            emit IncreasedLock();
        }
    }

    /// ============ AUTOMATE ============ ///
    /**@notice Create automation for Reoccuring Bribes and/or claiming and revoting every week
     * @param _bribeTokens Array of an array ok tokens by pool they will be bribed too.
     * @param _pools Array of pools you would like to bribe.
     * @param _amountToFundThisContractPerPool How much would you like to transfer to this contract by pool to fund bribes.
     * @param _weeklyAmounts How much would you like to bribe per pool per token.
     * @param _noBribe If we just want to automate claiming and voting without bribes then this is true.
     * @param _withClaimAllAndVote If we want to automate claiming and voting in addition to bribes then this is true.
     */
    function createAutomation(
        address[][] calldata _bribeTokens,
        address[] calldata _pools,
        uint256[][] calldata _amountToFundThisContractPerPool,
        uint256[][] calldata _weeklyAmounts,
        bool _noBribe,
        bool _withClaimAllAndVote
    ) external payable onlyOwner {
        if (automationActive) _stopAutomation(false);
        if (!_noBribe) {
            for (uint i; i < _pools.length; ++i) {
                address[] memory _tokens = _bribeTokens[i];
                for (uint j; j < _tokens.length; ++j) {
                    if (!bribeTokenExists[_tokens[j]])
                        _initBribeToken(_tokens[j]);
                    IERC20(_tokens[j]).safeTransferFrom(
                        msg.sender,
                        address(this),
                        _amountToFundThisContractPerPool[i][j]
                    );
                }
            }
        }

        reoccuringBribe = ReoccuringBribe(
            _pools,
            _bribeTokens,
            _weeklyAmounts,
            _noBribe,
            _withClaimAllAndVote
        );
        _createTask();
        automationActive = true;
    }

    function _createTask() private {
        ModuleData memory moduleData = ModuleData({
            modules: new Module[](2),
            args: new bytes[](2)
        });

        moduleData.modules[0] = Module.RESOLVER;
        moduleData.modules[1] = Module.PROXY;

        moduleData.args[0] = abi.encode(
            address(this),
            abi.encodeCall(this.canExecute, ())
        );
        moduleData.args[1] = bytes("");

        currentTaskId = ops.createTask(
            address(this),
            abi.encode(this.execute.selector),
            moduleData,
            MATIC
        );
    }

    function execute() external onlyGelato {
        if (!reoccuringBribe.noBribe) {
            if (reoccuringBribe.withClaimAndVote) _claimAllAndVote();
            for (uint i; i < reoccuringBribe.pools.length; ++i) {
                address[] memory _tokens = reoccuringBribe.tokens[i];
                for (uint j; j < _tokens.length; ++j) {
                    _bribe(
                        _tokens[j],
                        reoccuringBribe.pools[i],
                        reoccuringBribe.amounts[i][j],
                        true
                    );
                }
            }
        }

        (uint256 feeAmount, ) = ops.getFeeDetails();
        if (address(this).balance < feeAmount) revert GelatoHungry();
        _payTxFee(feeAmount);
        lastExecution = block.timestamp;
    }

    function _payTxFee(uint256 _amount) private {
        address gelato = ops.gelato();
        (bool success, ) = gelato.call{value: _amount}("");
        if (!success) revert NativeTransferFailed();
    }

    function canExecute()
        external
        view
        returns (bool canExec, bytes memory execPayload)
    {
        execPayload = abi.encodeWithSignature("execute()");
        if (block.timestamp - lastExecution > WEEK) canExec = true;
        else {
            canExec = false;
            execPayload = bytes("");
        }
    }

    /// ============ Manage ============ ///
    function depositTokens(
        address _token,
        uint256 _amount
    ) external payable onlyOwner {
        if (_amount > 0)
            IERC20(_token).safeTransferFrom(msg.sender, address(this), _amount);
    }

    function withdrawTokens(address _token, bool _isNative) external onlyOwner {
        if (_isNative) {
            (bool success, ) = msg.sender.call{value: address(this).balance}(
                ""
            );
            if (!success) revert NativeTransferFailed();
        } else {
            uint256 tokenBal = IERC20(_token).balanceOf(address(this));
            IERC20(_token).safeTransfer(msg.sender, tokenBal);
        }
    }

    function setShouldMaxLock(bool _shouldMaxLock) external onlyOwner {
        shouldMaxLock = _shouldMaxLock;
    }

    function stopAutomation(bool _withdraw) external onlyOwner {
        _stopAutomation(_withdraw);
    }

    function _stopAutomation(bool _withdraw) private {
        if (_withdraw) {
            uint256 bal = address(this).balance;
            if (bal > 0) {
                (bool success, ) = msg.sender.call{value: bal}("");
                if (!success) revert NativeTransferFailed();
            }

            for (uint i; i < reoccuringBribe.pools.length; ++i) {
                address[] memory _tokens = reoccuringBribe.tokens[i];
                for (uint j; j < _tokens.length; ++j) {
                    bal = IERC20(_tokens[j]).balanceOf(address(this));
                    if (bal > 0)
                        IERC20(_tokens[j]).safeTransfer(msg.sender, bal);
                }
            }
        }

        ops.cancelTask(currentTaskId);

        delete reoccuringBribe;
        delete currentTaskId;
        lastExecution = 0;
        automationActive = false;
    }

    function _approveTokenIfNeeded(address token, address spender) private {
        if (IERC20(token).allowance(address(this), spender) == 0) {
            IERC20(token).safeApprove(spender, type(uint256).max);
        }
    }

    receive() external payable {}
}
